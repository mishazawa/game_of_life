PImage hand;
PImage razor;

ParticleSystem blood;

ArrayList<PVector> cuts;

void setup() {
  noCursor();
  size(640, 360);
  hand = loadImage("hand.png");
  razor = loadImage("razor.png");
  cuts = new ArrayList<PVector>();
}

void draw() {
  background(255);

  pushMatrix();
  imageMode(CENTER);
  translate(width / 2, height - hand.width / 2);
  rotate(radians(90));
  image(hand, 0, 0);
  popMatrix();

  for (PVector cut: cuts) {
    pushMatrix();
    strokeWeight(2);
    stroke(random(200, 255), 0, 0, 255);
    line(cut.x, cut.y, cut.x + 10, cut.y+random(0, 3));
    popMatrix();
  }

  pushMatrix();
  imageMode(CENTER);
  translate(mouseX, mouseY);
  razor.resize(50, 40);

  if (mousePressed) {
    rotate(radians(-45));
  }

  image(razor, 0, 0);
  popMatrix();

  if (blood != null) {
    PVector gravity = new PVector(0, 1);
    PVector wind = new PVector(0.1, 0);

    blood.applyForce(gravity);
    blood.applyForce(wind);
    blood.applyAtrraction();
    blood.display();
  }


}

void mouseClicked () {
  if (mouseX < 351 && mouseX > 306 && mouseY > height - (hand.width - 50)) {
    color c = get(mouseX - 20, mouseY + 20);
    if (c != -263173) {
      blood = new ParticleSystem(mouseX, mouseY);
      for (int i = 0; i < 10; i++) {
        blood.update();
      }
      cuts.add(new PVector(mouseX - 20, mouseY + 20));
    }
  }
}
